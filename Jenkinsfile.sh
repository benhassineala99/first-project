pipeline {
    agent any

	
	 tools {
       maven 'M3'
       jdk 'java 1.8'
   }
	
    
    stages {
        stage('Build') {
            steps {
		withMaven {
                sh 'mvn clean install'
		}
            }
        }
        
    }   
}
